﻿using Remitano.PLC.AdminServiceSvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remitano.PLC.Interfaces
{
    public interface IAdminPLC
    {
        bool InsertAdmin(AdminBO admin);
    }
}
