﻿using Remitano.PLC.AdminServiceSvc;
using Remitano.PLC.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remitano.PLC
{
    public class AdminPLC : IAdminPLC
    {
        AdminServiceSvcClient service = new AdminServiceSvcClient();
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public bool InsertAdmin(AdminBO admin)
        {
          //  string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            try
            {
                bool isResult = service.InsertAdmin(admin);
                return isResult;
            }
            catch (Exception ex)
            {
                return false;
              //  Utility.WriteLog(fileLog, "Exception PLC Insert Admin : " + ex.Message);
            }
        }
    }
}
