﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Remitano.PLC
{
    public class Utility
    {
        public static void WriteLog(string path, string message)
        {

            if (!File.Exists(path))
            {
                using (StreamWriter w = File.CreateText(path))
                {
                    Log(message, w);
                }
            }
            else
            {
                using (StreamWriter w = File.AppendText(path))
                {
                    Log(message, w);
                }
            }
        }
        private static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\n" + DateTime.Now.ToString());
            w.WriteLine(" {0}", logMessage);
            w.WriteLine("-----------------------------------");
        }
    }
}
