﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace AdminRemitano.Models
{
    public class Utilitys
    {
        public static string EncodeString(string value)
        {
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5;
            md5 = new MD5CryptoServiceProvider();
            originalBytes = ASCIIEncoding.Default.GetBytes(value);
            encodedBytes = md5.ComputeHash(originalBytes);
            return BitConverter.ToString(encodedBytes);
        }
        public static string MaHoaMD5(string pass)
        {
            try
            {

                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] data = Encoding.UTF8.GetBytes(pass);
                data = md5.ComputeHash(data);
                StringBuilder buider = new StringBuilder();
                foreach (byte b in data)
                {
                    buider.Append(b.ToString("x2"));
                }
                return buider.ToString();
            }
            catch
            {
                return pass;
            }
        }



        public static DateTime ConvertStringToDateTime(string strDate)
        {
            try
            {
                string day = strDate.Substring(0, strDate.IndexOf("/"));
                if (day.Length == 1)
                {
                    day = "0" + day;
                    strDate = "0" + strDate;
                }
                day = day.Replace("/", "");
                string temp = strDate.Replace(day + "/", "");
                if (temp.Length == 4)
                {
                    temp = day + "/" + temp;
                }
                // string month = temp.Substring(0, strDate.IndexOf("/"));
                string month = temp.Substring(0, temp.IndexOf("/"));
                month = month.Replace("/", "");
                string year = temp.Replace(month + "/", "");
                year = year.Replace("/", "");
                string strFormat = month + "/" + day + "/" + year;
                return DateTime.Parse(strFormat);

            }
            catch
            {
                return DateTime.Parse("01/01/0001");
            }
        }

        public static string modHost = ConfigurationSettings.AppSettings["adminhost"];
        public static string userHost = ConfigurationSettings.AppSettings["DomainWebsite"];
        public static int CharsPerProductName = 28;

        public static String ConvertTo_NoSoundVietnamese(string accented)
        {
            if (accented == null || accented == "")
                return "";

            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");

            string strFormD = accented.Normalize(System.Text.NormalizationForm.FormD);
            return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static String RemoveSpecialChars(String str)
        {
            StringBuilder buffer = new StringBuilder(str.Length);
            foreach (char ch in str)
                if (('0' <= ch && ch <= '9') || ('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z') || (ch == '-' || ch == '_' || ch == ' '))
                    buffer.Append(ch);
            return buffer.ToString();
        }
        public static string sefWord(String str)
        {
            if (str.Length > CharsPerProductName)
                str = str.Substring(0, CharsPerProductName);

            return HttpUtility.UrlEncode(RemoveSpecialChars(ConvertTo_NoSoundVietnamese(str).ToLower().Trim().Replace(' ', '-')));
        }
        public static string sefNewsPromotionLink(string categoryUrl, int newsID, string title)
        {
            return String.Format(userHost + "/{0}/{1}-{2}.html", categoryUrl, newsID, sefWord(title).ToLower());

        }
    }
}