﻿using AdminRemitano.Models;
using Remitano.PLC;
using Remitano.PLC.AdminServiceSvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminRemitano.Controllers
{
    public class AdminController : Controller
    {
        AdminPLC service = new AdminPLC();
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add(string adminID)
        {
            int id = 0;
            Int32.TryParse(adminID, out id);
            ViewData["AdminID"] = id;
            return View();
        }

        public ActionResult CreateNewAdmin(HttpPostedFileBase avatarImage)
        {
            string newDomainImage = ConfigurationManager.AppSettings["AvatarImages"];
            string cmd = "";
            int adminID = int.Parse(Request["hdAdminID"]);
            string userName = Request["txtUserName"];
            string password = Request["txtPassword"];
            string address = Request["txtAddress"];
            string mobile = Request["txtMobile"];
            string email = Request["txtEmail"];
            string fullName = Request["txtFullName"];
            int gender = int.Parse(Request["radioGender"]);
            string birthday = Request["txtBirthday"];
            string avatar = Request["hdAvatar"];
            if (avatarImage != null)
            {
                string pic = System.IO.Path.GetFileName(avatarImage.FileName);
                Random rand = new Random();
                int number = rand.Next(100000);
                string fileName = System.DateTime.Now.ToString() + number.ToString() + "" + System.DateTime.Now.Ticks;
                string file = Utilitys.EncodeString(fileName + pic) + ".jpg";

                //string subfolder = DateTime.Now.Year.ToString();
                //string childFolder = DateTime.Now.Month.ToString();
                //string childSubFolder = DateTime.Now.Day.ToString();

                string subfolder = string.Format("{0:yyyy}", System.DateTime.Now);
                string childFolder = string.Format("{0:MM}", System.DateTime.Now);
                string childSubFolder = string.Format("{0:dd}", System.DateTime.Now);

                string subFolder = newDomainImage + "/" + subfolder + "/" + childFolder + "/" + childSubFolder + "/";
                if (!Directory.Exists(subFolder))
                    Directory.CreateDirectory(subFolder);

                //string subfolder = string.Format("{0:MM-yyyy}", System.DateTime.Now);
                //string folder = newDomainImage + "/" + subfolder;
                //if (!Directory.Exists(folder))
                //    Directory.CreateDirectory(folder);

                string path = System.IO.Path.Combine(
                                       subFolder, file);
                // file is uploaded
                avatarImage.SaveAs(path);

                // save the image path path to the database or you can send image
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    avatarImage.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                avatar = "/AvatarImages/" + subfolder + "/" + childFolder + "/" + childSubFolder + "/" + file;

            }
            string passMD5 = Utilitys.MaHoaMD5(password);

            AdminBO admin = new AdminBO();
            admin.Address = address;
            admin.Avatar = avatar;
            admin.Birthday = Utilitys.ConvertStringToDateTime(birthday);
            admin.CreatedDate = DateTime.Now;
            admin.CreatedUser = "";
            admin.Email = email;
            admin.FullName = fullName;
            admin.IsActive = true;
            admin.IsDeleted = false;
            admin.Mobile = mobile;
            admin.Password = passMD5;
            admin.UserName = userName;
           
            if (adminID == 0)
            {
                bool isResult = service.InsertAdmin(admin);
                if (isResult)
                {
                    cmd = "AddSuccess";
                }
            }
            else
            {
                //bool rs = ProjectPlanBusiness.UpdateProjectPlan(project, projectID, ConnectionType.AdminConnection);
                //if (rs)
                //{
                //    cmd = "UpdateSuccess";
                //}
            }

            Response.Redirect("/Admin");

            return View();
        }
    }
}