/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';

   
    config.clipboard_defaultContentType = 'text';

    //config.dialog_noConfirmCancel = false;

    config.dialog_startupFocusTab = true;
    config.disableNativeSpellChecker = true;


    config.entities = false;
    config.basicEntities = false;
    config.entities_greek = false;
    config.entities_latin = false;
    config.htmlEncodeOutput = false;
    config.entities = false;
    config.ForceSimpleAmpersand = true;

    config.extraPlugins = 'youtube,album,video,imagenew';
    // config.extraPlugins = "newplugin";
    //  config.removeDialogTabs = 'flash:advanced;image:Link';
    config.youtube_width = '640';
    config.youtube_height = '480';
   // config.extraPlugins = 'image';
    //config.extraPlugins = 'album';
    config.filebrowserUploadUrl = '/News/news/SaveUploadedFileNews';
    config.allowedContent = true
};
