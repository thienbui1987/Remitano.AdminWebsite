﻿CKEDITOR.plugins.add('imagenew',
{
    init: function (editor) {
        editor.addCommand('AddImageToCKedittor', new CKEDITOR.dialogCommand('AddImageToCKedittor'));
        editor.ui.addButton('Imagenew',
        {
            label: 'Insert a hình ảnh',
            command: 'AddImageToCKedittor',
            icon: this.path + 'images/imagethumb.png'
        });

        CKEDITOR.dialog.add('AddImageToCKedittor', function (editor) {

            var strhtml = '';
            strhtml = '<div class="modal-body"><div class="panel part-green-light padding-10" style="margin-bottom:0px; background-color:#efffe6; padding:10px 15px !important; border:1px solid #ddd"><form class="form-inline" >';
            strhtml += '<button type="submit" onclick="return uploadNoflash()" class="btn btn-success" style="padding:4px 12px; border-radius:2px; font-family:Roboto Condensed, Arial, sans-serif; font-size:13px; line-height:20px; color:#fff; background-color:#69b342; border-color:#4cae4c; position:relative; left:30px; top:3px; cursor:pointer; z-index:999; width:80px"><i class="fa fa-search" style="margin-right:6px; font:normal normal normal 14px/1 FontAwesome"></i>Đăng ảnh</button><input name="file" id="fileInput" type="file" class="btn btn-default btn-upload-img" style="width:100px;position: relative;top: 0px;left: -70px;z-index: 1;" multiple=""><img id="imgLoadingSearch" src="/Images/jive-image-loading.gif" style="display:none"></form></div>';
            strhtml += '<div class="panelBody padding-15" style="padding:15px 15px !important"><div class="table-responsive" style="height:350px; width:100%; text-align:center">';
            strhtml += '<img id="imgCkeditor" style="max-width:650px; max-height:350px"></div></div>';
            strhtml += '<button  onclick="return AddImageToCkeditor()" value="Đồng ý" class="btn btn-success" style="padding:4px 12px; border-radius:2px; font-family:Roboto Condensed, Arial, sans-serif; font-size:13px; line-height:20px; color:#fff; background-color:#69b342; border-color:#4cae4c; cursor:pointer"><i class="fa fa-check" style="margin-right:6px; font:normal normal normal 14px/1 FontAwesome; content:"\f00c""></i>Đồng ý</button></div>';

            //var lstVideo = $('#hdVideoList').val();
            //var domain = $('#hdDomainImage').val();
            //if (lstVideo != ''&&lstVideo!=null) {
            //    var arrLst = lstVideo.split(';');
            //    if (arrLst.length > 0) {
            //        strhtml = '<div style="height:330px; overflow-y:auto" ><ul style="margin:0 auto; width:100%" id="ulListImage">';
            //        for (var i = 0; i < arrLst.length; i++) {
            //            strhtml += '<li style="float:left; margin:10px; cursor:pointer"><img src=' + domain + arrLst[i] + ' title=' + domain + arrLst[i] + ' onclick="AddVideoToCkeditor(this.title)" style="width:100px; height:100px; cursor:pointer"></li>';
            //        }
            //        strhtml += '</ul></div>';
            //    }
            //}
            //$.ajax({
            //    type: "post",
            //    url: "/news/newstemp/GetNewsTempDetail",
            //    async: true,
            //    data: { newsID: newsID },
            //    beforeSend: function (d) {
            //        $('#imgLoading').css("display", "");
            //    },
            //    success: function (data2) {

            //    }
            //});
            return {
                title: 'Thêm hình ảnh',
                minWidth: 700,
                minHeight: 400,
                contents:
                [
                    {
                        id: 'general',
                        label: 'Settings',
                        elements:
                        [
                            {
                                type: 'html',
                                html: strhtml
                            }
                        ]
                    }
                ]
            };
        });
    }
});