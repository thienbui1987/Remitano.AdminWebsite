﻿CKEDITOR.plugins.add('video',
{
    init: function (editor) {
        editor.addCommand('ShowListVideoFile', new CKEDITOR.dialogCommand('ShowListVideoFile'));
        editor.ui.addButton('Video',
        {
            label: 'Insert a video',
            command: 'ShowListVideoFile',
            icon: this.path + 'images/video.png'
        });

        CKEDITOR.dialog.add('ShowListVideoFile', function (editor) {

            var strhtml = '';
            strhtml = '<div class="modal-body"><div class="panel part-green-light padding-10" style="margin-bottom:0px; background-color:#efffe6; padding:10px 15px !important; border:1px solid #ddd"><form class="form-inline" >';
            strhtml += '<div class="form-group" style="display:inline-block; margin-bottom:0px"><div class="input-group" style="display:inline-table"><input type="text" class="form-control" id="txtKeywordFile" name="txtKeywordFile" placeholder="Gõ từ khóa tìm kiếm..." style="padding:5px 10px; background-color:#fff; border:1px solid #ddd; font-size;13px; line-height:1.478"></div>';
            strhtml += '</div><button type="submit" onclick="return CheckSearchFileVideo()" class="btn btn-success" style="padding:4px 12px; border-radius:2px; font-family:Roboto Condensed, Arial, sans-serif; font-size:13px; line-height:20px; color:#fff; background-color:#69b342; border-color:#4cae4c; position:relative; left:30px; top:3px; cursor:pointer"><i class="fa fa-search" style="margin-right:6px; font:normal normal normal 14px/1 FontAwesome"></i>Tìm kiếm</button><img id="imgLoadingSearch" src="/Images/jive-image-loading.gif" style="display:none"></form></div>';
            strhtml += '<div class="panelBody padding-15" style="padding:15px 15px !important"><div class="table-responsive" style="height:350px; width:100%"><table class="table table-hover table-bordered-outside table-even" style="width:100%"><thead><tr><th style="background-color:#f5f5f5; border-bottom:1px solid #e5e5e5; color:#444; border-right:1px solid #e5e5e5; padding:10px">Chọn</th><th style="background-color:#f5f5f5; border-bottom:1px solid #e5e5e5; color:#444; border-right:1px solid #e5e5e5; padding:10px">Mã File</th><th style="background-color:#f5f5f5; border-bottom:1px solid #e5e5e5; color:#444; border-right:1px solid #e5e5e5; padding:10px">Tên file</th><th style="background-color:#f5f5f5; border-bottom:1px solid #e5e5e5; color:#444; border-right:1px solid #e5e5e5; padding:10px">Người tạo</th><th style="background-color:#f5f5f5; border-bottom:1px solid #e5e5e5; color:#444; border-right:1px solid #e5e5e5; padding:10px">Ngày tạo</th></tr></thead>';
            strhtml += '<tbody id="bodyVideoFileSerach"></tbody></table></div></div><div id="divPagingVideoFileSearch"> </div>';
            strhtml += '<button  onclick="return AddFileToNews()" value="Đồng ý" class="btn btn-success" style="padding:4px 12px; border-radius:2px; font-family:Roboto Condensed, Arial, sans-serif; font-size:13px; line-height:20px; color:#fff; background-color:#69b342; border-color:#4cae4c; cursor:pointer"><i class="fa fa-check" style="margin-right:6px; font:normal normal normal 14px/1 FontAwesome; content:"\f00c""></i>Đồng ý</button></div>';

            //var lstVideo = $('#hdVideoList').val();
            //var domain = $('#hdDomainImage').val();
            //if (lstVideo != ''&&lstVideo!=null) {
            //    var arrLst = lstVideo.split(';');
            //    if (arrLst.length > 0) {
            //        strhtml = '<div style="height:330px; overflow-y:auto" ><ul style="margin:0 auto; width:100%" id="ulListImage">';
            //        for (var i = 0; i < arrLst.length; i++) {
            //            strhtml += '<li style="float:left; margin:10px; cursor:pointer"><img src=' + domain + arrLst[i] + ' title=' + domain + arrLst[i] + ' onclick="AddVideoToCkeditor(this.title)" style="width:100px; height:100px; cursor:pointer"></li>';
            //        }
            //        strhtml += '</ul></div>';
            //    }
            //}
            //$.ajax({
            //    type: "post",
            //    url: "/news/newstemp/GetNewsTempDetail",
            //    async: true,
            //    data: { newsID: newsID },
            //    beforeSend: function (d) {
            //        $('#imgLoading').css("display", "");
            //    },
            //    success: function (data2) {

            //    }
            //});
            return {
                title: 'Danh sách file video',
                minWidth: 700,
                minHeight: 400,
                contents:
                [
                    {
                        id: 'general',
                        label: 'Settings',
                        elements:
                        [
                            {
                                type: 'html',
                                html: strhtml
                            }
                        ]
                    }
                ]
            };
        });
    }
});