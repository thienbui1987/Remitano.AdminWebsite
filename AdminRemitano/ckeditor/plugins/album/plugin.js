﻿CKEDITOR.plugins.add('album',
{
    init: function (editor) {
        editor.addCommand('ShowListAlbum', new CKEDITOR.dialogCommand('ShowListAlbum'));
        editor.ui.addButton('Album',
        {
            label: 'Insert a album',
            command: 'ShowListAlbum',
            icon: this.path + 'images/picture.jpg'
        });

        CKEDITOR.dialog.add('ShowListAlbum', function (editor) {

            var strhtml = '';

          
            var lstImage = $('#hdImageList').val();
            var domain = $('#hdDomainImage').val();
            if(lstImage!='')
            {
                var arrLst=lstImage.split(';');
                if(arrLst.length>0)
                {
                    strhtml = '<div style="height:330px; overflow-y:auto" ><ul style="margin:0 auto; width:100%" id="ulListImage">';
                    for(var i=0;i<arrLst.length;i++)
                    {
                        strhtml += '<li style="float:left; margin:10px; cursor:pointer"><img src=' + domain + arrLst[i] + ' title=' + domain+arrLst[i] + ' onclick="AddImageToCkeditor(this.title)" style="width:100px; height:100px; cursor:pointer"></li>';
                    }
                    strhtml+='</ul></div>';
                }
            }
            //$.ajax({
            //    type: "post",
            //    url: "/news/newstemp/GetNewsTempDetail",
            //    async: true,
            //    data: { newsID: newsID },
            //    beforeSend: function (d) {
            //        $('#imgLoading').css("display", "");
            //    },
            //    success: function (data2) {
                   
            //    }
            //});
            return {
                title: 'Danh sách ảnh bài viết',
                minWidth: 700,
                minHeight: 400,
                contents:
                [
                    {
                        id: 'general',
                        label: 'Settings',
                        elements:
                        [
                            {
                                type: 'html',
                                html: strhtml
                            }
                        ]
                    }
                ]
            };
        });
    }
});