﻿$(document).ready(function () {
    $("#basic_validate").validate({
        rules: {
            txtUserName: {
                required: true
                // txtCategoryName: true
            },
            txtPassword: {
                required: true
                // txtCateUrl: true
            },
            txtFullName: {
                required: true
                // txtCateUrl: true
            },
            txtEmail: {
                required: true
                // txtCateUrl: true
            },
            txtMobile: {
                required: true
                // txtCateUrl: true
            }
        },
        errorClass: "help-inline",
        errorElement: "span",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
            $(element).parents('.control-group').addClass('success');
        }
    });
});

function ShowImage(imageID, imagePreview, divImage, icon) {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById(imageID).files[0]);

    oFReader.onload = function (oFREvent) {
        $('#' + divImage).css("display", "none");
        $('#' + imagePreview).css("display", "block");
        $('#' + icon).css("display", "block");
        document.getElementById(imagePreview).src = oFREvent.target.result;
    };

}

function RemoveImage(id, inputFile, img, div) {
    document.getElementById(inputFile).value = null;
    $('#' + id).css("display", "none");
    $('#' + img).css("display", "none");
    $('#' + img).attr("src", "/Scripts/UploadFile/no_image.jpg");
    $('#' + div).css("display", "block");
}
