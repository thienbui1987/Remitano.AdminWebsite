﻿function LoginPage() {
    var checkReg = true;
    var userName = $('#txtUserName').val();
    var password = $('#txtPassword').val();
    if (userName == '' && password=='') {
        checkReg = false;
        $('#divMessage').text('Chưa nhập tên tài khoản và mật khẩu');

    }
    else if (userName == '') {
        checkReg = false;
        $('#divMessage').text('Chưa nhập tên tài khoản');

    }
    else if (password == '') {
        checkReg = false;
        $('#divMessage').text('Chưa nhập mật khẩu');

    }
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/Login/LoginSystem",
            data: { userName: userName, password: password },
            beforeSend: function (s) {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'loginsuccess') {
                    window.location.href = "/ManageMember";
                    $('#txtUserName').val('');
                    $('#txtPassword').val('');
                }
                else if(d=='account_not_active')
                {
                    noty({ text: "Tên đăng nhập hoặc mật khẩu không hợp lệ", layout: "bottomRight", type: "error" });
                    $('#divMessage').text('');
                }
                else {
                    noty({ text: "Tài khoản chưa được kích hoạt", layout: "bottomRight", type: "error" });
                    $('#divMessage').text('');
                }
            }
        });
    }
}
function EnterLogin(event, value) {
    if (event.which == 13 || event.keyCode == 13) {
        var checkReg = true;
        var userName = $('#txtUserName').val();
        var password = $('#txtPassword').val();
        if (userName == '' && password == '') {
            checkReg = false;
            $('#divMessage').text('Chưa nhập tên tài khoản và mật khẩu');

        }
        else if (userName == '') {
            checkReg = false;
            $('#divMessage').text('Chưa nhập tên tài khoản');

        }
        else if (password == '') {
            checkReg = false;
            $('#divMessage').text('Chưa nhập mật khẩu');

        }
        if (!checkReg) {
            return false;
        }
        else {
            $.ajax({
                type: "post",
                async: true,
                url: "/Login/LoginSystem",
                data: { userName: userName, password: password },
                beforeSend: function (s) {
                    showLoading();
                },
                success: function (d) {
                    hideLoading();
                    if (d == 'loginsuccess') {
                        window.location.href = "/ManageMember";
                        $('#txtUserName').val('');
                        $('#txtPassword').val('');
                    }
                    else if (d == 'account_not_active') {
                        noty({ text: "Tên đăng nhập hoặc mật khẩu không hợp lệ", layout: "bottomRight", type: "error" });
                        $('#divMessage').text('');
                    }
                    else {
                        noty({ text: "Tài khoản chưa được kích hoạt", layout: "bottomRight", type: "error" });
                        $('#divMessage').text('');
                    }
                }
            });
        }
    }

}

function ResetField(id) {
    switch (id) {
        case 1:
            {
                $('#lbUserName').css("display", "none");
                $('#lbUserName').text('');
                break;
            }
        case 2:
            {
                $('#lbPassword').css("display", "none");
                $('#lbPassword').text('');
                break;
            }
    }
}

function LogoutMember() {
    $.ajax({
        type: "post",
        url: "/Login/LogoutMember",
        async: true,
        data: {},
        beforeSend: function () {
            //$('#imgLoading').css("display", "");
        },
        success: function (d) {
            window.location.href = '/login';
        },
        error: function () {

        }
    });
}